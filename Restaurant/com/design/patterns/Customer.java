package com.design.patterns;

import java.util.Scanner;

public class Customer {

    private FirstFloorChef firstFloorChef;
    private SecondFloorChef secondFloorChef;

    Customer() {
        firstFloorChef = new FirstFloorChef();
        secondFloorChef = new SecondFloorChef();
    }
 
    public static void main(String[] args) {

        Customer customer = new Customer();
        Chef chef = customer.getCustomerMealsPreference();
        Command mealOrder = new MealOrder(chef);
        mealOrder.order(); 

        chef = customer.getCustomerDessertPreference();
        Command dessertOrder = new DessertOrder(chef);
        dessertOrder.order();
    }

    public Chef getCustomerMealsPreference() {

        System.out.println("Options Available : 1. South Meal  2. North Meal");
        Scanner in = new Scanner(System.in);
        int preference = in.nextInt();

        if (preference == 1)
            return firstFloorChef;
         else
            return secondFloorChef;
    }

    public Chef getCustomerDessertPreference() {
    
        System.out.println("Options Available : 1. Mysuru Pak 2. Jamun");
        Scanner in = new Scanner(System.in);
        int preference = in.nextInt();

        if (preference == 1)
            return firstFloorChef;
         else
            return secondFloorChef;
    }
}