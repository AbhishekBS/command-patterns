package com.design.patterns;
 
public interface Chef {
 
    void prepareMeal();
    void prepareDessert();
}