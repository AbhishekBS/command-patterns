package com.design.patterns;

public class DessertOrder implements Command {
 
    private Chef chef;
     
    public DessertOrder(Chef chef){
        this.chef=chef;
    }

    @Override
    public void order() {
        this.chef.prepareDessert();
    }
}