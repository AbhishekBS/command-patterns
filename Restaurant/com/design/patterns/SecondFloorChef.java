package com.design.patterns;
 
public class SecondFloorChef implements Chef {
 
    @Override
    public void prepareMeal() {
        System.out.println("Preparing North Meal");
    }
 
    @Override
    public void prepareDessert() {
        System.out.println("Preparing Jamun");
    }
}