package com.design.patterns;

public class MealOrder implements Command {
 
    private Chef chef;
     
    public MealOrder(Chef chef){
        this.chef=chef;
    }

    @Override
    public void order() {
        this.chef.prepareMeal();
    }
}