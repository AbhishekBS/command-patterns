package com.design.patterns;
 
public class FirstFloorChef implements Chef {
 
    @Override
    public void prepareMeal() {
        System.out.println("Preparing South Meal");
    }
 
    @Override
    public void prepareDessert() {
        System.out.println("Preparing Mysuru Pak");
    }
}