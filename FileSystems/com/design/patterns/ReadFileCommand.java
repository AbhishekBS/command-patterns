package com.design.patterns;

public class ReadFileCommand implements Command {
 
    private FileSystemReceiver fileSystem;
     
    public ReadFileCommand(FileSystemReceiver fs){
        this.fileSystem=fs;
    }

    @Override
    public void execute() {
        this.fileSystem.readFile();
    }
}