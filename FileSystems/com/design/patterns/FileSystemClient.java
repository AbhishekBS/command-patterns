package com.design.patterns;

public class FileSystemClient {
 
    public static void main(String[] args) {

        FileSystemReceiver fsReceiver = FileSystemClient.getUnderlyingFileSystem();
        ReadFileCommand readFileCommand = new ReadFileCommand(fsReceiver);
        readFileCommand.execute(); 

         
        WriteFileCommand writeFileCommand = new WriteFileCommand(fsReceiver);
        writeFileCommand.execute();
         
    }

    private static FileSystemReceiver getUnderlyingFileSystem() {
        String osName = System.getProperty("os.name");
        System.out.println("Underlying OS is: " + osName);

        if (osName.contains("Windows"))
             return new WindowsFileSystemReceiver();
         else if (osName.contains("Mac OS X"))
             return new MacFileSystemReceiver();
         else
            return new UnixFileSystemReceiver();
    }
}