package com.design.patterns;
 
public class MacFileSystemReceiver implements FileSystemReceiver {
 
    @Override
    public void readFile() {
        System.out.println("Reading file in Mac OS");
    }
 
    @Override
    public void writeFile() {
        System.out.println("Writing file in Mac OS");
    }
}