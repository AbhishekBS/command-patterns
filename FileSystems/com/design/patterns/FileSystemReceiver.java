package com.design.patterns;
 
public interface FileSystemReceiver {
 
    void readFile();
    void writeFile();
}