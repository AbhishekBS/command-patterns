package com.design.patterns;
 
public class UnixFileSystemReceiver implements FileSystemReceiver {
 
    @Override
    public void readFile() {
        System.out.println("Reading file in unix OS");
    }
 
    @Override
    public void writeFile() {
        System.out.println("Writing file in unix OS");
    }
 }