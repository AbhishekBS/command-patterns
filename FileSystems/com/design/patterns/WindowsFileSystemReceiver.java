package com.design.patterns;
 
public class WindowsFileSystemReceiver implements FileSystemReceiver {
 
    @Override
    public void readFile() {
        System.out.println("Reading file in Windows OS");
    }
 
    @Override
    public void writeFile() {
        System.out.println("Writing file in Windows OS");
    }
}