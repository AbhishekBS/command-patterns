package com.design.patterns;

public interface Command {
 
    void execute();
}